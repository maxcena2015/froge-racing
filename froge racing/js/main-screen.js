window.addEventListener('load', function() {
    const tetrisRacingGame = new TetrisRacingGame('game-canvas', 'game-resources');
    tetrisRacingGame.run();
    setSoundIconClass(tetrisRacingGame.soundEnabled);
    getSoundIconElement().addEventListener('click', () => setSoundIconClass(tetrisRacingGame.toggleSound()));
});

function setSoundIconClass(soundEnabled) {
    const ON_CLASS = 'on';
    const OFF_CLASS = 'off';
    if (soundEnabled) {
        getSoundIconElement().classList.add(ON_CLASS);
        getSoundIconElement().classList.remove(OFF_CLASS);
        let soundtrack = document.querySelector("#frog-sound");
            soundtrack.play();
    } else {
        getSoundIconElement().classList.remove(ON_CLASS);
        getSoundIconElement().classList.add(OFF_CLASS);
        let soundtrack = document.querySelector("#frog-sound");
            soundtrack.pause();
    }
}

function getSoundIconElement() {
    return document.getElementById('sound-icon');
}
