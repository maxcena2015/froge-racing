/* ========================= Anna Borisenko creature block start=====================*/
let game = document.querySelector('.game');

let scores = 0;
//блок отображения очков
function createScoresBlock () {
    let scoresBlock = document.createElement("div");
        scoresBlock.className="scores-container";
    let h1 = document.createElement("h1");
        h1.innerText="Scores: Х";
        scoresText = document.createElement("span");
        scoresText.innerText=scores;
        h1.appendChild(scoresText);
        scoresBlock.appendChild(h1);
        game.appendChild(scoresBlock);        
}
//блок окончания игрі с кнопками доната и перезапуска игры
function generateGameOverText() {
    let gameOverText = document.createElement("div");
        gameOverText.className ="game-over-text";
    let h1 = document.createElement("h1");
        h1.innerText="YOU NEVER LOOSE WITH FROGE, TRY HARDER, CHAD!";
    let button = document.createElement("buttton");
        button.id = "copyAddress";
        button.innerText = "ERC20 FROGE: и адрес димы";
    let egeinButton = document.createElement("buttton");
        egeinButton.id = "egeinButton";
        egeinButton.innerText = "Try egein";
    gameOverText.appendChild(h1);
    gameOverText.appendChild(button);
    gameOverText.appendChild(egeinButton);
    game.appendChild(gameOverText);
    egeinButton.onclick=restart;

}
//перезапуск игры
function restart () {
	location.reload ();
};
//блок формирования надписей переключения скоростей

function createNextLevelText () {
    let textLevel = document.getElementById('score-title');
     if(scores==30) {
            textLevel.innerText="hold rainforests";
        } else if(scores==60) {
            textLevel.innerText="you are frogesome!";
        } else if(scores==100) {
            textLevel.innerText="almost froge friday!";
        } else if(scores==200) {
            textLevel.innerText="Frogellionaire!";
        } else if(scores==500) {
            textLevel.innerText="Kill that zero!";
        } else if(scores==600) {
           textLevel.innerText="Based Winner gets the dinner!";
        } else if(scores==900) {
            textLevel.innerText="Diamond Fucking Hands Baby!";
        } else if(scores==1000) {
            textLevel.innerText="Rose loves you!";
        } else {
            textLevel.innerText="";
        }

     
}


/* ========================= Anna Borisenko creature block end=====================*/
//запускается игра
const TetrisRacingGame = (function() {
    //константа положения 
    const _state = new WeakMap();
    //константа контекста - нужна для отображения илюстраций-надписей окончания игры и перехода на следующий уровень
    const _ctx = new WeakMap();
    //константа используется для указания ресурсо того или иного объекта
    const _resources = new WeakMap();
    //задает, на какой интервал увеличивается скорость
    const _interval = new WeakMap();
    //константа отслеживания переключения уровней и увеличения скорости  
    const _levelChangeListener = new WeakMap();
    //константа сокращения записи TetrisRacingGameConstants для обращения к необходимой глобальной константе
    const CONST = TetrisRacingGameConstants;
   

    class TetrisRacingGame {
        
        //формирует игровое поле и элементы на нем
        constructor(canvasId, resourcesId, levelChangeListener = () => {}) {
           
            createScoresBlock ();
            _state.set(this, getDefaultState());
            _ctx.set(this, document.getElementById(canvasId).getContext('2d'));
            _levelChangeListener.set(this, levelChangeListener);
            _resources.set(this, readResources(resourcesId));
            addKeyDownEventListener.call(this);
        }
        
        //обеспечивает непрерывное движение вперед 
        run() {
             
            const {state, levelChangeListener} = _get(this);
            state.currentLevelLines = generateLevelLines();
            state.running = true;
            levelChangeListener(state.level);
            _interval.set(this, setInterval(processCurrentFrame.bind(this), CONST.INTERVAL_TIMEOUT));
        }

        //управление звуком
        toggleSound() {
          
            const {state} = _get(this);
            return state.soundEnabled = !state.soundEnabled;
        }

        get soundEnabled() {
            return _state.get(this).soundEnabled;
        }
    }

    //задает отслеживание каждого объекта
    function _get(obj) {
        return {          
            state: _state.get(obj),
            ctx: _ctx.get(obj),
            resources: _resources.get(obj),
            interval: _interval.get(obj),
            levelChangeListener: _levelChangeListener.get(obj)
        };       
    }

    function getDefaultState() {
        return {
            currentLevelLines: [],
            level: 1,
            progress: 0,
            player: {
                position: 1
            },
            running: false,
            soundEnabled: false
        };
    }

    function readResources(resourcesId) {
        const resources = {};
        
        document.querySelectorAll(`#${resourcesId} > *`).forEach(resource => {
            resources[resource.getAttribute('data-name')] = resource;
        });
        return resources;
    }
    
    //определяет положение игрока на линии при переходе на следующий уровень
    function generateLevelLines() {
        const lines = [];
       
        for (let i = 0; i <= CONST.NUMBER_OF_LINES * 2 + 1; i++) {
            if (i % 2 === 0 && i < CONST.NUMBER_OF_LINES*2) {
               
                lines.push(CONST.POSSIBLE_LEVEL_LINES[Math.floor(Math.random() * CONST.POSSIBLE_LEVEL_LINES.length)]);
            } else {
                lines.push(CONST.EMPTY_LINE);
            }
        }
        return lines;
    }

    function playSound(sound) {
        if (_state.get(this).soundEnabled) {
            sound.play();
        }
    }

    function processCurrentFrame() {
        const playerData = calculatePlayerData.call(this);
       
        const currentLineHeight = calculateCurrentLineHeight.call(this);
        const opponentRectangles = calculateOpponentRectangles.call(this, currentLineHeight);
        clearContext.call(this);
        drawPlayer.call(this, playerData);
        drawOpponentCars.call(this, opponentRectangles);
        drawWalls.call(this);
        
        Dogproshol(opponentRectangles, playerData)


        if (isGameOverDetected(opponentRectangles, playerData)) {
            scoresText.innerText=scores;
            processGameOver.call(this);
        } else if (isNextLevelDetected.call(this, currentLineHeight)) {
            processNextLevel.call(this);
        } else {
            increaseProgress.call(this);
        }
    }

    function clearContext() {
        const {ctx} = _get(this);
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    }

    function calculatePlayerData() {
        const {state, ctx} = _get(this);
        return {
            x: CONST.WALL_WIDTH_MARGIN + state.player.position * CONST.CAR_WIDTH_MARGIN,
            y: ctx.canvas.height - CONST.CAR_HEIGHT_MARGIN,
            width: CONST.CAR_WIDTH_NO_MARGIN,
            height: CONST.CAR_HEIGHT_NO_MARGIN
        }
    }

    function drawPlayer(playerData) {
        const {ctx, resources} = _get(this);
        ctx.drawImage(resources[CONST.PLAYER_CAR_IMG], playerData.x, playerData.y);
    }

    function calculateOpponentRectangles(currentLineHeight) {
        const {ctx, state} = _get(this);
        const lines = state.currentLevelLines;
        const opponentRectangles = [];
        for (let i = 0; i < lines.length; i++) {
            let y = i * currentLineHeight - (lines.length * currentLineHeight) + ctx.canvas.height + Math.round(state.progress);
            if (y > -CONST.CAR_HEIGHT_MARGIN && y < ctx.canvas.height) {
                const lineElements = lines[i];
                for (let j = 0; j < lineElements.length; j++) {
                    if (lines[i][j] === 1) {
                        const opponentRectangle = {
                            x: CONST.WALL_WIDTH_MARGIN + j * CONST.CAR_WIDTH_MARGIN,
                            y: y,
                            width: CONST.CAR_WIDTH_NO_MARGIN,
                            height: CONST.CAR_HEIGHT_NO_MARGIN
                        };
                        opponentRectangles.push(opponentRectangle);
                    }
                }
            }
        }
        return opponentRectangles;
    }

    function drawOpponentCars(opponentRectangles) {
        const {ctx, resources} = _get(this);
        opponentRectangles.forEach(opponentRectangle => ctx.drawImage(resources[CONST.OPPONENT_CAR_IMG], opponentRectangle.x, opponentRectangle.y));
    }

    function isGameOverDetected(opponentRectangles, playerData) {
        for (let i = 0; i < opponentRectangles.length; i++) {
            if (CollisionDetector.detectCollision(playerData, opponentRectangles[i])) {
                console.log(opponentRectangles[i]);
                console.log(playerData);
                return true;
            }
        }
        return false;
    }
    function Dogproshol(opponentRectangles, playerData) {
     
        for (let i = 0; i < opponentRectangles.length; i++) {
             
            if (CollisionDetector.detectEND(playerData, opponentRectangles[i])) {
                scores = scores + 30;
                scoresText.innerText = scores;
                console.log("sdafsad");
            }else  {
                if(scores >220){
                    // console.log(opponentRectangles[i]);
                    // console.log(playerData);
                    // return;
                }
            }
        }
    }
    //тут задано условие переключения уровней и увеличения скорости
    function isNextLevelDetected(currentLineHeight) {
        let {state} = _get(this);
        if(scores >= 30 && state.level == 1) {

            return true;
        } else if(scores >= 60 && state.level == 2) {
             return true;
        } else if(scores >= 100 && state.level == 3) {
            return true;
        } else if(scores >= 200 && state.level == 4) {
            return true;
        } else if(scores >= 500 && state.level == 5) {
            return true;
        } else if(scores >= 600 && state.level == 6) {
            return true;
        } else if(scores >= 900 && state.level == 7) {
            return true;
        } else if(scores >= 1000 && state.level == 8) {
            return true;
        }
          return false;
        // return state.progress > state.currentLevelLines.length * currentLineHeight;
    }

    function addKeyDownEventListener() {
        const {state, resources} = _get(this);
        let canv = document.getElementById("game-canvas");
        var that = this;
        canv.addEventListener('touchstart', function(e){
            if (!state.running) return;
            if(e.changedTouches[0].pageX < e.target.clientWidth/2) {
               if (state.player.position > 0) {
                    state.player.position--;
                    playSound.call(that, resources[CONST.MOVE_SOUND]);
                }
            } else {
                if (state.player.position < 2) {
                    state.player.position++;
                    playSound.call(that, resources[CONST.MOVE_SOUND]);
                }
            }
        }, false);

        window.addEventListener('keydown', event => {
            if (!state.running) return;
            if (event.code === CONST.ARROW_LEFT) {
                event.preventDefault();
                if (state.player.position > 0) {
                    state.player.position--;
                    playSound.call(this, resources[CONST.MOVE_SOUND]);
                }
            } else if (event.code === CONST.ARROW_RIGHT) {
                event.preventDefault();
                if (state.player.position < 2) {
                    state.player.position++;
                    playSound.call(this, resources[CONST.MOVE_SOUND]);
                }
            }
        });
    }

    // function resetState() {
    //     const {state} = _get(this);
    //     Object.assign(state, (({running, level, progress, player: {position}}) =>
    //         ({running, level, progress, player: {position: position}}))(getDefaultState()));
    // }
    //функция перехода к следующему уровню
    function prepareStateToNextLevel() {
        const {state} = _get(this);
        state.level++;
        Object.assign(state, (({running, progress, player: {position}}) =>
            ({running, progress, player: {position: position}}))(getDefaultState()));
    }
    //прорисовка боковых стенок в виде тетрисных стенок и движение собачек вниз
    function drawWalls() {
        const {ctx, state, resources} = _get(this);
        const minPosition = -Math.ceil(1.5 * state.currentLevelLines.length * calculateCurrentLineHeight.call(this));
        for (let i = minPosition; i < ctx.canvas.height; i += CONST.WALL_HEIGHT_MARGIN) {
            const y = i +  Math.round(1.5 * state.progress);
            if (y > -CONST.WALL_HEIGHT_MARGIN && y < ctx.canvas.height) {
                ctx.drawImage(resources[CONST.WALL_IMG], 0, y);
                ctx.drawImage(resources[CONST.WALL_IMG], ctx.canvas.width - CONST.WALL_WIDTH_NO_MARGIN, y);
            }
        }
    }

    //выводит на экран сообщение об окончании игры
    function processGameOver() {
        const {ctx, interval, resources} = _get(this);
        clearInterval(interval);
        generateGameOverText();
        playSound.call(this, resources[CONST.GAME_OVER_SOUND]);
        // resetState.call(this);
      
    }

    //выводит сообщения при достижении следующего уровня и ускроряет движение
    function processNextLevel() {
        const {ctx, interval, resources} = _get(this);
        clearInterval(interval);
        createNextLevelText ();
        prepareStateToNextLevel.call(this);
        setTimeout(this.run.bind(this), CONST.PAUSE_TIMEOUT);
                        
    }

    function increaseProgress() {
        const {state} = _get(this);
        state.progress += (2 + state.level * 2) * (24/CONST.FPS);
    }

    function calculateCurrentLineHeight() {
        return CONST.BASE_LINE_HEIGHT + Math.ceil(_state.get(this).level * 0.033 * CONST.BASE_LINE_HEIGHT);
    }

    return TetrisRacingGame;
})();
